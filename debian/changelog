libcatmandu-filestore-perl (1.16-3) unstable; urgency=medium

  * Team upload.
  * Bump versioned test and runtime dependency on libcatmandu-perl.
    Thanks to Santiago Vila for the bug report. (Closes: #1093340)
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 17 Jan 2025 20:14:24 +0100

libcatmandu-filestore-perl (1.16-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on liblog-any-perl,
      libtest-exception-perl, libtest-simple-perl.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Jonas Smedegaard ]
  * update watch file: set version mangling

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 10 Dec 2022 14:49:38 +0000

libcatmandu-filestore-perl (1.16-1) unstable; urgency=medium

  * Team upload.

  * Import upstream version 1.16.
    + no longer depends on Data::UUID (Closes: #955689)
    + fixes build issues with recent Catmandu versions
  * Update build and runtime dependencies based on upstream declarations.

 -- Niko Tyni <ntyni@debian.org>  Fri, 22 May 2020 21:20:17 +0300

libcatmandu-filestore-perl (1.13-2) unstable; urgency=medium

  * Team upload.

  [ Jonas Smedegaard ]
  * Add hint that package enhances recent libcatmandu-perl.
  * Install upstream README.

  [ gregor herrmann ]
  * Fix and extend autopkgtests.
    - add debian/tests/pkg-perl/smoke-files: t/Catmandu-Cmd-stream.t needs
      'cpanfile'
    - add empty debian/tests/pkg-perl/syntax-skip to eanble syntax.t although
      the package has Suggests

 -- gregor herrmann <gregoa@debian.org>  Tue, 29 Jan 2019 18:41:16 +0100

libcatmandu-filestore-perl (1.13-1) unstable; urgency=low

  * Initial Release.
    Closes: bug#920612.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 27 Jan 2019 14:10:51 +0100
