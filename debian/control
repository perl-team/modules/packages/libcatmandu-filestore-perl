Source: libcatmandu-filestore-perl
Section: perl
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 libcatmandu-perl (>= 1.2024) <!nocheck>,
 libclone-perl <!nocheck>,
 libcpanel-json-xs-perl <!nocheck>,
 libhash-merge-simple-perl <!nocheck>,
 libio-callback-perl <!nocheck>,
 libio-string-perl <!nocheck>,
 liblog-any-adapter-log4perl-perl,
 liblog-any-perl <!nocheck>,
 liblog-log4perl-perl,
 libmodule-build-perl (>= 0.422900),
 libmoo-perl <!nocheck>,
 libnamespace-clean-perl <!nocheck>,
 libpackage-stash-perl <!nocheck>,
 libpath-iterator-rule-perl <!nocheck>,
 libpath-tiny-perl <!nocheck>,
 librole-tiny-perl <!nocheck>,
 libtest-deep-perl <!nocheck>,
 libtest-exception-perl <!nocheck>,
 libtest-lwp-useragent-perl <!nocheck>,
 libtest-pod-perl <!nocheck>,
 libtest-simple-perl <!nocheck>,
 liburi-perl <!nocheck>,
 perl,
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcatmandu-filestore-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcatmandu-filestore-perl
Homepage: https://librecat.org/Catmandu/
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-perl

Package: libcatmandu-filestore-perl
Architecture: all
Depends:
 libcatmandu-perl (>= 1.2024),
 libclone-perl,
 libhash-merge-simple-perl,
 libio-string-perl,
 libmoo-perl,
 libnamespace-clean-perl,
 libpackage-stash-perl,
 libpath-iterator-rule-perl,
 libpath-tiny-perl,
 liburi-perl,
 ${misc:Depends},
 ${perl:Depends},
Suggests:
 liblog-any-adapter-log4perl-perl,
 liblog-log4perl-perl,
Enhances:
 libcatmandu-perl (>= 1.1000),
Replaces:
 libcatmandu-perl (<< 1.1000~),
Breaks:
 libcatmandu-perl (<< 1.1000~),
Description: modules to make files persistent within the Catmandu framework
 Each Catmandu::FileStore is a Catmandu::Store and inherits all its methods.
 .
 A Catmandu::FileStore is package to store and retrieve binary content
 in an filesystem, memory or a network.
 A Catmandu::FileStore contains one or more Catmandu::FileBag
 which is a kind of folder.
 .
 Each Catmandu::FileBag contains one or more files.
 .
 One special Catmandu::FileBag is the index
 which contains the listing of all Catmandu::FileBag
 in the Catmandu::FileStore.
 .
 Catmandu provides a suite of Perl modules
 to ease the import, storage, retrieval, export
 and transformation of metadata records.
